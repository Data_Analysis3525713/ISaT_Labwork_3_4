import pandas as pd
import numpy as np
from sklearn.manifold import TSNE
import umap as umap
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import cm
from sklearn.preprocessing import MinMaxScaler, StandardScaler, RobustScaler
from sklearn.preprocessing import (
    MinMaxScaler,
    RobustScaler,
    StandardScaler,
    minmax_scale
)

# function loading data and getting y = dataset.target 
def prepare_data (path: str, return_type="ndArray"):
    X = np.genfromtxt(path, delimiter=",")
    y = np.array([])

    for i in X:
        y = np.append(y, i[5])
        
    X = np.delete(X, 5, 1)
    if return_type == "ndArray": return X, y
    if return_type == "dataframe": 
        X = pd.DataFrame(data=X)
        return X, y
    if return_type == "list":
        X = X.tolist()
        return X, y

# Vizualising dataset
def _vizualise (result, y, algname=''):
    plt.figure(figsize=(8, 6))
    scatter = plt.scatter(result[:, 0], result[:, 1], c=y, cmap="viridis", alpha=0.8)

    cbar = plt.colorbar(scatter, ticks=range(3))
    cbar.ax.set_yticklabels(['1', '2', '3']) 
    
    title = "Hayes-Roth Dataset " + algname + " Visualization"
    plt.title(title, fontsize="15")
    plt.xlabel(algname + " 1st Component")
    plt.ylabel(algname + " 2nd Component")
    plt.show()

# performing UMAP and vizualising it
def draw_umap(X, y, n_neighbors=15, min_dist=0.1, n_components=2, metric='euclidean', scaler=''):
    # Performing UMAP
    fit = umap.UMAP(
        n_neighbors=n_neighbors,
        min_dist=min_dist,
        n_components=n_components,
        metric=metric
    )
    u = fit.fit_transform(X)
    
    # Vizualising results
    if scaler != '': scaler = ' with ' + scaler
    _vizualise(u, y, algname=("UMAP" + scaler))

def draw_t_SNE (X, y, scaler=''):
    # Perform t-SNE
    X_tsne = TSNE(n_components=2).fit_transform(X)

    # Vizualise the results
    if scaler != '': scaler = ' with ' + scaler
    _vizualise(X_tsne, y, algname=("t-SNE" + scaler))


def create_axes(title, figsize=(16, 6)):
    fig = plt.figure(figsize=figsize)
    fig.suptitle(title)

    # define the axis for the first plot
    left, width = 0.1, 0.22
    bottom, height = 0.1, 0.7
    bottom_h = height + 0.15
    left_h = left + width + 0.02

    rect_scatter = [left, bottom, width, height]
    rect_histx = [left, bottom_h, width, 0.1]
    rect_histy = [left_h, bottom, 0.05, height]

    ax_scatter = plt.axes(rect_scatter)
    ax_histx = plt.axes(rect_histx)
    ax_histy = plt.axes(rect_histy)

    # define the axis for the zoomed-in plot
    left = width + left + 0.2
    left_h = left + width + 0.02

    rect_scatter = [left, bottom, width, height]
    rect_histx = [left, bottom_h, width, 0.1]
    rect_histy = [left_h, bottom, 0.05, height]

    ax_scatter_zoom = plt.axes(rect_scatter)
    ax_histx_zoom = plt.axes(rect_histx)
    ax_histy_zoom = plt.axes(rect_histy)

    # define the axis for the colorbar
    left, width = width + left + 0.13, 0.01

    rect_colorbar = [left, bottom, width, height]
    ax_colorbar = plt.axes(rect_colorbar)

    return (
        (ax_scatter, ax_histy, ax_histx),
        (ax_scatter_zoom, ax_histy_zoom, ax_histx_zoom),
        ax_colorbar,
    )


def plot_distribution(axes, X, y, hist_nbins=50, title="", x0_label="", x1_label=""):
    ax, hist_X1, hist_X0 = axes

    ax.set_title(title)
    ax.set_xlabel(x0_label)
    ax.set_ylabel(x1_label)

    # The scatter plot
    colors = cmap(y)
    ax.scatter(X[:, 0], X[:, 1], alpha=0.5, marker="o", s=5, lw=0, c=colors)

    # Removing the top and the right spine for aesthetics
    # make nice axis layout
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()
    ax.spines["left"].set_position(("outward", 10))
    ax.spines["bottom"].set_position(("outward", 10))

    # Histogram for axis X1 (feature 5)
    hist_X1.set_ylim(ax.get_ylim())
    hist_X1.hist(
        X[:, 1], bins=hist_nbins, orientation="horizontal", color="grey", ec="grey"
    )
    hist_X1.axis("off")

    # Histogram for axis X0 (feature 0)
    hist_X0.set_xlim(ax.get_xlim())
    hist_X0.hist(
        X[:, 0], bins=hist_nbins, orientation="vertical", color="grey", ec="grey"
    )
    hist_X0.axis("off")

def make_plot(item_idx):
    title, X = distributions[item_idx]
    ax_zoom_out, ax_zoom_in, ax_colorbar = create_axes(title)
    axarr = (ax_zoom_out, ax_zoom_in)
    plot_distribution(
        axarr[0],
        X,
        y,
        hist_nbins=200,
        x0_label=feature_mapping[features[0]],
        x1_label=feature_mapping[features[1]],
        title="Full data",
    )

    # zoom-in
    zoom_in_percentile_range = (0, 99)
    cutoffs_X0 = np.percentile(X[:, 0], zoom_in_percentile_range)
    cutoffs_X1 = np.percentile(X[:, 1], zoom_in_percentile_range)

    non_outliers_mask = np.all(X > [cutoffs_X0[0], cutoffs_X1[0]], axis=1) & np.all(
        X < [cutoffs_X0[1], cutoffs_X1[1]], axis=1
    )
    plot_distribution(
        axarr[1],
        X[non_outliers_mask],
        y[non_outliers_mask],
        hist_nbins=50,
        x0_label=feature_mapping[features[0]],
        x1_label=feature_mapping[features[1]],
        title="Zoom-in",
    )

    norm = mpl.colors.Normalize(y_full.min(), y_full.max())
    mpl.colorbar.ColorbarBase(
        ax_colorbar,
        cmap=cmap,
        norm=norm,
        orientation="vertical",
        label="Color mapping for values of y",
    )
    plt.show()


# ---------- Here comes the main code ----------
X, y = prepare_data("./hayes+roth/hayes-roth.data", "ndArray")

# Vizualisation without scalers
draw_umap(X, y, n_components=4, scaler='No scaler')
draw_t_SNE(X, y, scaler='No scaler')

# Vizualisation after using MinMaxScaler
X_mmsc = MinMaxScaler().fit_transform(X)
draw_umap(X_mmsc, y, n_components=4, scaler='MinMaxScaler')
draw_t_SNE(X_mmsc, y, scaler='MinMaxScaler')

# Vizualisation after using StandardScaler
X_ssc = StandardScaler().fit_transform(X)
draw_umap(X_ssc, y, n_components=4, scaler='StandardScaler')
draw_t_SNE(X_ssc, y, scaler='StandardScaler')

# Vizualisation after using RobustScaler
X_rsc = RobustScaler().fit_transform(X)
draw_umap(X_rsc, y, n_components=4, scaler='RobustScaler')
draw_t_SNE(X_rsc, y, scaler='RobustScaler')


# Comparision the effect of different scalers on data with outliers 
X_full, y_full = prepare_data("./hayes+roth/hayes-roth.data", "ndArray")
feature_names = ['hobby', 'age', 'education_level', 'martial_status']

feature_mapping = {
    "hobby": "Hobby",
    "age": "Age",
    "education_level": "Educational level",
    "martial_status": "Martial Status"
}

features = ["hobby", "age"]
features_idx = [feature_names.index(feature) for feature in features]
X = []

for i in range(len(X_full)):
    X.append([])
    for j in features_idx:
        X[i].append(X_full[i][j])

distributions = [
    ("Unscaled data", X),
    ("Data after standard scaling", StandardScaler().fit_transform(X)),
    ("Data after min-max scaling", MinMaxScaler().fit_transform(X)),
    (
        "Data after robust scaling",
        RobustScaler(quantile_range=(25, 75)).fit_transform(X),
    )
]

# scale the output between 0 and 1 for the colorbar
y = minmax_scale(y_full)

# plasma does not exist in matplotlib < 1.5
cmap = getattr(cm, "plasma_r", cm.hot_r)

# MinMax
make_plot(2)
# Standard
make_plot(1)
# Robust
make_plot(3)